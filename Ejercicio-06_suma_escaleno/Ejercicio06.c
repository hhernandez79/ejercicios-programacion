#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv)
{
    float lado1 = 0;
    float lado2 = 0;
    float lado3 = 0;   
    float perimetro = 0;

    printf("Ingrese el tamaño del primer lado del triangulo escaleno: \n");

    scanf("%f", &lado1);

    if (lado1 <= 0)
    {
        printf("El tamaño del lado no pueder ser igual o menor que 0");
    }
    
    else
    {
        printf("Ingrese el tamaño del segundo lado del triangulo escaleno: \n");

        scanf("%f", &lado2);

        if (lado2 <= 0)
        {
            printf("El tamaño del lado no pueder ser igual o menor que 0");

        }

        else 
        {
            printf("Ingrese el tamaño del tercer lado del triangulo escaleno: \n");

            scanf("%f", &lado3);

            if (lado3 <= 0)
            {
                 printf("El tamaño del lado no pueder ser igual o menor que 0");
            }

            if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3)
            {
                printf("Los lados del triangulo escaleno no pueden ser iguales. Revise los datos");

            }
            else
            {
                 perimetro = lado1 + lado2 + lado3;

                printf("\nEl perimetro del triangulo es %0.2f", perimetro);
            }
       
        }
    }
        

    return 0;


}