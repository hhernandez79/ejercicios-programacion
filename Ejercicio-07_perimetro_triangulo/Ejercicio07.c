#include <stdio.h>
#include <stdlib.h>

int menu();
void equilatero();
void isosceles();
void escaleno();
void clearBuffer();

int main(int argc, char** argv) 
{

    menu();

    return(EXIT_SUCCESS);

    
    
}   


int menu()
{
    char c;

    while(c != 4)
    {  
    
        printf("\n\n\tMENU: ");
        printf("\nELIJA EL TIPO DE TRIANGULO PARA REALIZAR EL CALCULO DEL PERIMETRO:");
        printf("\n 1. Triangulo Equilatero");
        printf("\n 2. Triangulo Isosceles");
        printf("\n 3. Triangulo Escaleno");
        printf("\n 4. Salir \n");

        c = getchar();

        switch(c) 
        {
            case '1':
                equilatero();
                break;
            case '2':
                isosceles();
                break;
            case '3':
                escaleno();
                break;
            case '4':
                exit(-1);
    
        } clearBuffer();    
    }
    return(1);
}

void clearBuffer() 
{
  while(getchar() != '\n');
}

void equilatero()
{
    float lado = 0;
    float perimetro = 0;

    printf("Ingrese el tamaño de los lados del triangulo equilatero: \n");

    scanf("%f", &lado);

    if (lado <= 0)
    {
       
        printf("El tamaño del lado no pueder ser igual o menor que 0");
    }

    else
    {

        perimetro = lado * 3;

        printf("\nEl perimetro del triangulo es %0.2f", perimetro);

    
    }

}

void isosceles()
{
    float ladoIgual = 0;
    float ladoDiferente = 0;
    float perimetro = 0;

    printf("Ingrese el tamaño de los lados iguales del triangulo isósceles: \n");

    scanf("%f", &ladoIgual);

    if (ladoIgual <= 0)
    {
        printf("El tamaño del lado no pueder ser igual o menor que 0");
    }
    
    else
    {
        printf("Ingrese el tamaño del lado diferente del triangulo isósceles: \n");

        scanf("%f", &ladoDiferente);

        if (ladoDiferente <= 0)
        {
            printf("El tamaño del lado no pueder ser igual o menor que 0");

        }

        else 
        {
        perimetro = (ladoIgual * 2) + ladoDiferente;

        printf("\nEl perimetro del triangulo es %0.2f", perimetro);
        }
    }
}

void escaleno()
{
    float lado1 = 0;
    float lado2 = 0;
    float lado3 = 0;   
    float perimetro = 0;

    printf("Ingrese el tamaño del primer lado del triangulo escaleno: \n");

    scanf("%f", &lado1);

    if (lado1 <= 0)
    {
        printf("El tamaño del lado no pueder ser igual o menor que 0");
    }
    
    else
    {
        printf("Ingrese el tamaño del segundo lado del triangulo escaleno: \n");

        scanf("%f", &lado2);

        if (lado2 <= 0)
        {
            printf("El tamaño del lado no pueder ser igual o menor que 0");

        }

        else 
        {
            printf("Ingrese el tamaño del tercer lado del triangulo escaleno: \n");

            scanf("%f", &lado3);

            if (lado3 <= 0)
            {
                 printf("El tamaño del lado no pueder ser igual o menor que 0");
            }

            if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3)
            {
                printf("Los lados del triangulo escaleno no pueden ser iguales. Revise los datos");

            }
            else
            {
                 perimetro = lado1 + lado2 + lado3;

                printf("\nEl perimetro del triangulo es %0.2f", perimetro);
            }
       
        }
    }

}