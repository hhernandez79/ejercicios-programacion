#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv)
{
    float ladoIgual = 0;
    float ladoDiferente = 0;
    float perimetro = 0;

    printf("Ingrese el tamaño de los lados iguales del triangulo isósceles: \n");

    scanf("%f", &ladoIgual);

    if (ladoIgual <= 0)
    {
        printf("El tamaño del lado no pueder ser igual o menor que 0");
    }
    
    else
    {
        printf("Ingrese el tamaño del lado diferente del triangulo isósceles: \n");

        scanf("%f", &ladoDiferente);

        if (ladoDiferente <= 0)
        {
            printf("El tamaño del lado no pueder ser igual o menor que 0");

        }

        else 
        {
        perimetro = (ladoIgual * 2) + ladoDiferente;

        printf("\nEl perimetro del triangulo es %0.2f", perimetro);
        }
    }
        

    return 0;


}