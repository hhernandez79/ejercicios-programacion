#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv)
{
    float lado = 0;
    float perimetro = 0;

    //double TIPO_CAMBIO = 19.92;

    printf("Ingrese el tamaño de los lados del triangulo equilatero: \n");

    scanf("%f", &lado);

    if (lado <= 0)
    {
       
        printf("El tamaño del lado no pueder ser igual o menor que 0");
    }

    else
    {

        perimetro = lado * 3;

        printf("\nEl perimetro del triangulo es %0.2f", perimetro);

    
    }

    return 0;


}