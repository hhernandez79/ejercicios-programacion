#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv) 
{
    int numero = 0;

    printf("Seleccione un número, del 2 al 10, para mostrar su tabla de multiplicar: \n");
    scanf("%i", &numero);

    for(int i = 1; i < 11; i++)
    {
        /*printf("%d", numero, " * ", i, " = ", numero * i, "\n");*/

        printf("\n%i * %i = %i", numero, i, numero * i);

    }


    return(EXIT_SUCCESS);

    
    
}   