
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv)
{
    int numero;
    int suma = 0;

    printf("Ingrese un número entre 1 y 50: \n");

    scanf("%d", &numero);

    if (numero > 50)
    {
        printf("El numero no puede ser mayor a 50");
    }

    else
    {

        for(int i = 0; i <= numero; i++)
        {
            suma += i;

        }

    printf("\nLa sumatoria de consecutivos hasta %d", numero);
    printf(" es: %d", suma);

    
    }

    return 0;


}