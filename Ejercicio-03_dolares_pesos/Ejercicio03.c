#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv)
{
    float pesos = 0;
    float dolares = 0;

    double TIPO_CAMBIO = 19.92;

    printf("Ingrese el número de dolares a intercambiar: \n");

    scanf("%f", &dolares);

    if (dolares <= 0)
    {
       
        printf("El numero de dolares a ingresar debe ser un número positivo");
    }

    else
    {

        pesos = dolares * TIPO_CAMBIO;

        printf("\nEl tipo de cambio actual es $%0.2f", TIPO_CAMBIO);
        printf("\nLa cantidad a entregar en pesos es $%0.2f", pesos);
    //printf(" es: %d", suma);

    
    }

    return 0;


}