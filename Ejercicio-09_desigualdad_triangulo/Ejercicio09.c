
#include <stdio.h>
#include <stdlib.h>

void comparar();

int main(int argc, char** argv) 
{
    float lados[3];
    
    printf("\n\tPrograma para validar el principio de la desigualdad del triangulo\n");

    printf("\nIngrese tres longitudes para verificar si es posible construir un triangulo");

    for (int i = 0; i < 3; i++)
    {
        printf("\nIngrese la longitud %i del triangulo\n", i + 1);
        scanf("%f", &lados[i]);

    }

    /*for (int i =0; i < 3; i++)
    {
        printf("\nLos datos ingresados son %f, %f, %f", lados[0], lados[1], lados[2]);
    }*/

    /*printf("\nLos datos ingresados son: \n");
    for (int i = 0; i < 3; i++)
    {
        printf("\n%0.2f", lados[i]);
    }*/

    comparar(lados);

    return(EXIT_SUCCESS);
    
}   

void comparar(float lados[])
{
     if(((lados[0] + lados[1]) > lados[2]) && ((lados[0] + lados[2]) > lados[1]) && ((lados[1] + lados[2]) > lados[0]))
    {
        printf("\n\nEs posible crear un triangulo con las longitudes ingresadas %0.2f, %0.2f, %0.2f", lados[0], lados[1], lados[2]);
    }
    else
    {
         printf("\n\nNo posible crear un triangulo con las longitudes ingresadas %0.2f, %0.2f, %0.2f", lados[0], lados[1], lados[2]);
         printf("\nNo se cumplen las tres desigualdades");
         
    }

}