
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) 
{
    int anio;

    printf("\tBienvenido al programa. Este identifica si un año es bisiesto");

    
    printf("\n\nIngrese un año (entre 1582 y 2022)\n");
    scanf("%d", &anio);

    if (anio < 1582 || anio > 2022)
    {
        printf("El año introducido está fuera del rango permitido");
    }

    else 
    {
        if (anio % 4 == 0)
        {
            printf("El año %d es bisiesto", anio);
        }
        else
        {
            printf("El año %d no es bisiesto", anio);

        }
    }


   





    return (EXIT_SUCCESS);
}